<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Painel extends CI_Controller {

	public $breadcrumb = array();

	public function __construct()
    {
        parent::__construct();
        
        $this->load->model('sessao_model', 'sessao', TRUE);
        $this->sessao->verificar_login();

        $this->load->model('user_model', 'user', TRUE);

        $this->breadcrumb = array();
    }

	/**
	 * Index Page for this controller.
	 * 
	 * Maps URL
	 * 	./painel
	 * 	./painel/index
	 */
	public function index()
	{
		$data = array(
			'title_page' => 'Painel'
		);
		$message = array();

		$breadcrumb = array();

		$this->load->view('templates/header', $data);
		
		$this->load->view('templates/navigation-left', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
		));
		$this->load->view('templates/before-content', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
			'breadcrumb' => $this->breadcrumb,
			'message' => $message,
		));
		$this->load->view('painel/index', array(
			'usuario' => $this->user->getInfo($this->sessao->userId())
		));
		$this->load->view('templates/after-content');
		$this->load->view('templates/footer');
	}


	

}