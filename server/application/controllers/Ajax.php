<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	

	public function __construct()
    {
        parent::__construct();
        
		$this->load->model('util_model', 'util');
		$this->load->model('login_model', 'login', TRUE);
		$this->load->model('ajax_model', 'ajax', TRUE);
		//$this->load->model('notificacao_model', 'notificacao', TRUE);

		

    }

	public function index()
	{
		

		$this->util->apiReturn();
	}






















	/**
	 * REFATORAR PARA CRIAR NOVOS USUARIOS
	 * 
	 * Maps URL
	 * ./api-front/cadastro-artistas/
	 * @param String convite, nome, email, senha
	 * @return Array [  ]
	 */
	public function cadastro_artistas()
	{
		$convite = $this->input->post('convite');
		$convite = strtoupper($convite);

		$nomeCompleto = $this->input->post('nome');
		$email = $this->input->post('email');
		$senha = $this->input->post('senha');

		if (empty($nomeCompleto) || empty($email) || empty($senha))
			return $this->_returnJSON(array(
				'action' => false,
				'message' => 'Preencha todas as informações'
			));

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return $this->_returnJSON(array(
				'action' => false,
				'message' => 'E-mail inválido'
			));

		if(!empty($this->api->verificar_pre_cadastro($email)))
			return $this->_returnJSON(array(
				'action' => false,
				'message' => 'E-mail já cadastrado, faça login em "minha loja"'
			));


		$nomeCompleto = explode(' ', $nomeCompleto);
		$nome = $nomeCompleto[0];
		unset($nomeCompleto[0]);
		$sobrenome = implode(' ', $nomeCompleto);

		$key = bin2hex($this->encryption->create_key(32));
		$token = '$' . crypt($email, $key) . '#!' . random_string('alnum', 20);
		$senha = base64_decode($senha);

		$data = array(
			'nome' => $nome,
			'sobrenome' => $sobrenome,
			'email' => $email,
			'convite' => $convite,
			'senha' => password_hash($senha, PASSWORD_BCRYPT),
			'token' => $token,
			'hash_key' => $key,
			'user_info' => $this->_user_info(),
			'date_create' => date('Y-m-d H:m:s')
		);

		$save = $this->api->save_pre_cadastro($data);

		if ($save) {
			
			$this->notificacao->email_admin_novo_cliente(array(
				'nome' => $nome . ' ' . $sobrenome,
				'email' => $email,
                'token' => $token,
			));
			$sendEmail = $this->notificacao->email_artista_cadastro($email, array(
                'nome' => $nome,
                'sobrenome' => $sobrenome,
                'token' => $token,
            ));

		} else {
			return $this->_returnJSON(array(
				'action' => false,
				'message' => 'Erro no servidor'
			));
		}

		
		return $this->_returnJSON(array(
			'action' => true,
			'message' => 'Cadastro feito, confirme o seu e-mail!',
			'action' => true,
			'save' => $save,
			'emailSend' => $sendEmail
		));

	}




	

}