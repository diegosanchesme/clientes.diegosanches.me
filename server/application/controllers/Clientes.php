<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

	public $breadcrumb = array();

	public function __construct()
    {
        parent::__construct();
        
        $this->load->helper('string');

        $this->load->model('sessao_model', 'sessao', TRUE);
        $this->sessao->verificar_login();

        $this->load->model('user_model', 'user', TRUE);
        $this->load->model('clientes_model', 'clientes', TRUE);

        $this->breadcrumb = array(
        	array(
        		'label' => 'clientes',
	    		'url' => base_url() . 'clientes.html',
	    		'current' => TRUE
        	)
        );
    }

	/**
	 * Maps URL
	 * 	./clientes
	 * 	./clientes/index
	 */
	public function index()
	{
		$data = array(
			'title_page' => 'Clientes'
		);
		$message = array();

		

		$this->load->view('templates/header', $data);
		
		$this->load->view('templates/navigation-left', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
		));
		$this->load->view('templates/before-content', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
			'breadcrumb' => $this->breadcrumb,
			'message' => $message,
		));
		
		$this->load->view('clientes/index', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
			'clientes' => $this->clientes->getAll()
		));

		$this->load->view('templates/after-content');
		$this->load->view('templates/footer');
	}





	/**
	 * Maps URL
	 * 	./clientes/novo
	 */
	public function detalhes()
	{
		$idCliente = (Int) $this->uri->segment(3);

		$data = array(
			'title_page' => 'Detalhes Cliente '
		);
		$message = array();

		$this->breadcrumb[] = array(
    		'label' => 'detalhe',
    		'url' => base_url('clientes/detalhe/'.$idCliente.'.html'),
    		'current' => TRUE
    	);

		$this->load->view('templates/header', $data);
		
		$this->load->view('templates/navigation-left', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
		));
		$this->load->view('templates/before-content', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
			'breadcrumb' => $this->breadcrumb,
			'message' => $message,
		));
		$this->load->view('clientes/detalhes', array(
			'usuario' => $this->user->getInfo($this->sessao->userId())
		));
		$this->load->view('templates/after-content');
		$this->load->view('templates/footer');
	}





	/**
	 * Maps URL
	 * 	./clientes/novo
	 */
	public function novo()
	{
		$data = array(
			'title_page' => 'Novo Cliente'
		);
		$message = array();

		$this->breadcrumb[] = array(
    		'label' => 'novo',
    		'url' => base_url('clientes/novo.html'),
    		'current' => TRUE
    	);

    	$post = $this->input->post();
    	
    	if(!empty($post)) {
    		$insert = $this->clientes->saveNew($post);
    		if($insert) {
    			$sendEmail = TRUE; // FAZER NOTIFICAÇÃO
    			$message = array(
	    			'type' => 'is-success',
	    			'text' => $post['nome'].' '.$post['sobrenome'].', foi adicionado com sucesso.'
	    		);
    		} else {
    			$message = array(
	    			'type' => 'is-danger',
	    			'text' => ''
	    		);
    		}
    	}



		$this->load->view('templates/header', $data);
		
		$this->load->view('templates/navigation-left', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
		));
		$this->load->view('templates/before-content', array(
			'usuario' => $this->user->getInfo($this->sessao->userId()),
			'breadcrumb' => $this->breadcrumb,
			'message' => $message,
		));
		$this->load->view('clientes/novo', array(
			'usuario' => $this->user->getInfo($this->sessao->userId())
		));
		$this->load->view('templates/after-content');
		$this->load->view('templates/footer');
	}
	

}