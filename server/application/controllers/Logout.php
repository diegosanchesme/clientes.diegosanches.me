<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model('sessao_model', 'sessao', TRUE);
        $this->sessao->finalizar_login();

    }

	/**
	 * Index Page for this controller.
	 * 
	 * Maps URL
	 * 	./register
	 * 	./register/index
	 */
	public function index()
	{
		
		
		$data = array(
			'title_page' => 'Logout'
		);

		$this->load->view('templates/header', $data);
		$this->load->view('auth/logout');
		$this->load->view('templates/footer');

	}


}