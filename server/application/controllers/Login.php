<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model('sessao_model', 'sessao', TRUE);
        $this->sessao->verificar_login_page();
        
        $this->load->model('util_model', 'util');
        $this->load->model('login_model', 'login', TRUE);
        $this->load->model('notificacao_model', 'notificacao', TRUE);
        
    }



	/**
	 * Index Page for this controller.
	 * 
	 * Maps URL
	 * 	./login
	 * 	./login/index
	 */
	public function index()
	{
		$param = array(
			'email' => (string) $this->input->get('e')
		);

		$data = array(
			'title_page' => 'Login'
		);

		$this->load->view('templates/header', $data);
		$this->load->view('auth/login-cliente', $param);
		$this->load->view('templates/footer');

	}



	/**
	 * Index Page for this controller.
	 * 
	 * Maps URL
	 * 	./login/master
	 */
	public function master()
	{
		$param = array(
			'email' => (string) $this->input->get('e')
		);

		$data = array(
			'title_page' => 'Login Master'
		);

		$this->load->view('templates/header', $data);
		$this->load->view('auth/login-master', $param);
		$this->load->view('templates/footer');

	}









	/**
	 * VALIDAR EMAIL
	 * 
	 * Maps URL
	 * ./ajax/validar-email/
	 * @param Array email
	 * @return Array [ email, name ]
	 */
	public function validar_email()
	{
		$this->sessao->finalizar_login();

		$email = $this->input->post('email');

		if (empty($email))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Preencha seu E-mail'
			));

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'E-mail inválido'
			));

		$verificaEmail = $this->login->verificar_email($email);

		if($verificaEmail) {
			$return = array(
				'action' => true,
				'user' => $verificaEmail
			);
		} else {
			$return = array(
				'action' => false,
				'message' => 'Seu e-mail não foi encontrado, verifique se ele está correto'
			);
		}
		$this->util->apiReturn($return);
	}





	/**
	 * VALIDAR EMAIL MASTER
	 * 
	 * Maps URL
	 * ./ajax/validar-email-master/
	 * @param Array email
	 * @return Array [ email, name ]
	 */
	public function validar_email_master()
	{
		$this->sessao->finalizar_login();

		$email = $this->input->post('email');

		if (empty($email))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Preencha seu E-mail'
			));

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'E-mail inválido'
			));

		$verificaEmail = $this->login->verificar_email_master($email);

		if($verificaEmail) {
			$return = array(
				'action' => true,
				'user' => $verificaEmail
			);
		} else {
			$return = array(
				'action' => false,
				'message' => 'Seu e-mail não foi encontrado, verifique se ele está correto'
			);
		}
		$this->util->apiReturn($return);
	}




	

	/**
	 * Index Page for this controller.
	 * 
	 * Maps URL
	 * 	./login/autenticar
	 */
	public function autenticar()
	{
		$email = $this->input->post('email');
		$senha = $this->input->post('senha');

		if (empty($email))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Preencha seu E-mail'
			));

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'E-mail inválido'
			));

		if (empty($senha))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Preencha sua Senha'
			));

		$verificaLogin = $this->login->verificar_login($email, $senha);

		if($verificaLogin === NULL)
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Usuário Bloqueado'
			));

		if($verificaLogin) {
			$this->sessao->iniciar($verificaLogin);
			return $this->util->apiReturn(array(
				'action' => true,
				'redirect' => base_url() . 'painel.html',
				'message' => 'Login feito com sucesso',
			));
		} else {
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Senha inválida'
			));
		}
	}


	

	/**
	 * Index Page for this controller.
	 * 
	 * Maps URL
	 * 	./login/autenticar
	 */
	public function autenticar_master()
	{
		$email = $this->input->post('email');
		$senha = $this->input->post('senha');

		if (empty($email))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Preencha seu E-mail'
			));

		if (!filter_var($email, FILTER_VALIDATE_EMAIL))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'E-mail inválido'
			));

		if (empty($senha))
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Preencha sua Senha'
			));

		$verificaLogin = $this->login->verificar_login($email, $senha);

		if($verificaLogin === NULL)
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Usuário Bloqueado'
			));

		if($verificaLogin) {
			$this->sessao->iniciar($verificaLogin);
			return $this->util->apiReturn(array(
				'action' => true,
				'redirect' => base_url() . 'painel.html',
				'message' => 'Login feito com sucesso',
			));
		} else {
			return $this->util->apiReturn(array(
				'action' => false,
				'message' => 'Senha inválida'
			));
		}
	}


	












}