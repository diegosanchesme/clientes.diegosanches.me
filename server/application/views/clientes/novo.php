<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section id="content" class="section">
	<div class="container">
		
		<div class="heading">
			<h1 class="title">
				<i class="fa fa-user-plus"></i>
				<span>Novo Cliente</span>
			</h1>
			<div id="timerNow">. . .</div>
			<hr>
		</div>

		<div class="box">
			<div class="content">
				<form id="formMeusDados" action="" method="post">
					<div class="field is-horizontal">
						<div class="field-label is-normal">
							<label class="label">Nome Completo</label>
						</div>
						<div class="field-body">
							<div class="field">
								<input id="" class="input" type="text" placeholder="Nome *" value="" name="nome" required>
							</div>
							<div class="field">
								<input id="" class="input" type="text" placeholder="Sobrenome *" value="" name="sobrenome" required>
							</div>
						</div>
					</div>
					<div class="field is-horizontal">
						<div class="field-label is-normal">
							<label class="label">E-mail Principal</label>
						</div>
						<div class="field-body">
							<div class="field">
								<input id="" class="input" type="email" placeholder="E-mail *" value="" name="email" required>
							</div>
						</div>
					</div>

					<div class="field is-horizontal">
						<div class="field-label is-normal">
							<label class="label">Contatos Telefônicos</label>
						</div>
						<div class="field-body">
							<div class="field">
								<input id="dadosPessoaisTelefone" class="input" type="text" placeholder="Telefone (99) 3333.3333" value="" name="telefone">
							</div>
							<div class="field">
								<input id="dadosPessoaisCelular" class="input" type="text" placeholder="Celular (99) 99999.9999" value="" name="celular">
							</div>
						</div>
					</div>

					<div class="field is-horizontal">
						<div class="field-label is-normal">
							<label class="label">Identificações Legais</label>
						</div>
						<div class="field-body">
							<div class="field">
								<input id="dadosPessoaisRg" class="input" type="text" placeholder="RG" value="" name="rg">
							</div>
							<div class="field">
								<input id="dadosPessoaisCPF" class="input" type="text" placeholder="CPF" value="" name="cpf">
							</div>
						</div>
					</div>


					<div class="field">
						<p class="control has-text-right">
							<button class="button is-medium is-success">S A L V A R</button>
						</p>
					</div>
				</form>
			</div>
		</div>

	</div>
</section>

<script type="text/javascript">
	$(window).on('load', function () {

		
		
		$('#dadosPessoaisRg').mask("99.999.999?-99");
		$('#dadosPessoaisCPF').mask("999.999.999-99");
		$('#dadosPessoaisTelefone').mask("(99) 9999.9999");
		$('#dadosPessoaisCelular').mask("(99) 99999.999?9");

	});
</script>