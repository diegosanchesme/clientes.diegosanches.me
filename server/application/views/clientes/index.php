<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section id="content" class="section">
	<div class="container">
		
		<div class="heading">
			<h1 class="title">
				<i class="fa fa-users"></i>
				<span>Clientes</span>
			</h1>
			<div id="timerNow">. . .</div>
			<hr>
		</div>
		<div class="field is-grouped">
			<p class="control">
				<a href="<?php echo base_url('clientes/novo.html');?>" class="button is-success">Novo Cliente</a>
			</p>
		</div>
		<div class="box">
			<div class="content">

			<?php if(!empty($clientes)) : ?>
				<table id="tableClientes" class="table is-striped">
					<thead>
					    <tr>
							<th style="width: 50px;">Foto</th>
							<th class="" style="width: 100px;">#ID</th>
							<th style="width: 200px;">Nome</th>
							<th style="width: 300px;">E-mail</th>
							<th>Serviços</th>
							<th class="has-text-centered" style="width: 90px;">Ações</th>
					    </tr>
					</thead>
					<tbody>
					  	<?php foreach($clientes as $key => $cliente) : ?>
					    <tr>
							<th class="has-text-left">
								<a href="<?php echo base_url('clientes/detalhes/'. $cliente->id_cliente .'.html');?>">
									<img src="<?php echo $cliente->foto; ?>" width="35" style="background-color: #FFF; width: 35px; border-radius: 100px;">
									<!-- <img src="https://www.gravatar.com/avatar/<?php echo md5($cliente->email); ?>" width="35" style="background-color: #FFF; width: 35px; border-radius: 100px;"> -->
								</a>
							</th>
							<th class="">#<?php echo $cliente->id_cliente; ?></th>
							<th>
								<?php echo $cliente->nome . ' ' . $cliente->sobrenome; ?></th>
							<th>
								<a href="" class="">
									<?php echo $cliente->email; ?>
								</a>
							</th>
							<th>
								<!-- <span class="tag">Hospedagem</span> -->
							</th>
							<th class="has-text-centered">
								<div class="dropdown is-right is-hoverable">
									<div class="dropdown-trigger">
										<button class="button" aria-haspopup="true" aria-controls="dropdown-menu<?php echo $key; ?>">
											<i class="fa fa-cog"></i>
											<span class="icon is-small">
												<i class="fa fa-angle-down" aria-hidden="true"></i>
											</span>
										</button>
									</div>
									<div class="dropdown-menu" id="dropdown-menu<?php echo $key; ?>" role="menu">
										<div class="dropdown-content has-text-left">
											<div class="dropdown-item">
												<p>
													<a href="<?php echo base_url('clientes/detalhes/'. $cliente->id_cliente .'.html');?>">Mais Detalhes</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</th>
					    </tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
			</div>
		</div>

	</div>
</section>
<style type="text/css">
	.box {
		padding: 2px;
	}
	.box .table {
		margin-bottom: 0;
	}
</style>
<script type="text/javascript">
	$(window).on('load', function () {

		//$('#tableClientes').DataTable();

	});
</script>