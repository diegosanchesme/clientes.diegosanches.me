<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section id="loginAdmin" class="hero is-fullheight">

	<div class="hero-head">
		<div class="container has-text-centered">
			<?php $this->load->view('templates/logo'); ?>
		</div>
	</div>

	<div class="hero-body">
		<div class="container has-text-centered">
			
			<form id="formLogin" action="javascript:void(0);" method="post">
				<div id="userInfo" style="display: none;">
					<img src="" width="100" height="100">
					<span></span>
				</div>
				<div class="notification" style="display: none;">
					<button type="button" class="delete close-notifications"></button>
					<p></p>
				</div>
				<div id="inputEmail" class="field">
					<p class="control has-icons-left has-icons-right is-medium">
						<input id="loginEmail" class="input is-medium" type="email" placeholder="Seu e-mail" autocomplete="off" value="<?php echo base64_decode($email); ?>">
						<span class="icon is-small is-left">
							<i class="fa fa-envelope"></i>
						</span>
						<span class="icon icon-email-valid is-small is-right" style="color: #3a8a01; display: none;">
							<i class="fa fa-check"></i>
						</span>
					</p>
				</div>
				<div id="inputSenha" class="field" style="display: none;">
					<p class="control has-icons-left">
						<input id="loginSenha" class="input is-medium" type="password" placeholder="Sua senha" autocomplete="off">
						<span class="icon is-small is-left">
							<i class="fa fa-lock"></i>
						</span>
					</p>
				</div>
				<div class="field is-grouped is-grouped-centered">
					<p class="control">
						<input id="submitLogin" type="submit" class="button is-medium" value="C O N T I N U A R">
					</p>
				</div>
				<p style="display: none;">
					<a href="#!/esqueci-minha-senha" class="">Esqueci minha senha</a>
				</p>
			</form>
			
		</div>
	</div>
</section>
<style type="text/css">
	#footer {
		display: none;
	}
	#engineLoad {
		display: none !important;
	}
	a {
		font-size: 14px;
		color: #999;
		letter-spacing: 1.5px;
	}
	a:hover {
		color: #CCC;
	}
	
	#loginAdmin {
		background: #000;
	    background: -webkit-linear-gradient(-220deg, #000, #000);
	    background: linear-gradient(-220deg, #000, #000);
	}
	#formLogin {
		width: 330px;
		margin: 0 auto;
	}
	#formLogin .notification {
	    margin-bottom: 0.75rem;
	}
	#formLogin #userInfo {}
	#formLogin #userInfo img {
		background-color: #777;
		display: inline-block;
		width: 100px;
		height: 100px;
		margin-bottom: 7px;
		border-radius: 100px;
	}
	#formLogin #userInfo span {
		font-size: 18px;
	    color: #999;
	    letter-spacing: 3px;
	    display: block;
	    margin-bottom: 12px;
	}
	#submitLogin {
		background-color: #FFCE00;
		font-weight: bold;
		color: #000;
		width: 330px;
		border: none;
	}
	#submitLogin:hover {
		background-color: #000;
		color: #FFCE00;
	}
	.sending #submitLogin {
		background-color: #777 !important;
		color: #999 !important;
		cursor: wait !important;
	}
	.button:focus:not(:active), .button.is-focused:not(:active),
	.input:focus, .input.is-focused, .input:active, .input.is-active {
	  border-color: rgba(0,0,0,0.5);
	  -webkit-box-shadow: none !important;
	          box-shadow: none !important;
	}
	.hero-foot {}
	.hero-foot a {
		display: inline-block;
		position: relative;
		top: -25px;
	}
	#botSpeech {
		display: none;
	}
</style>
<script type="text/javascript">
	$(document).on('ready', function() {

		if(!isMobile.any) {
			$('#loginEmail').focus();
		}
		if(Lockr.get('userInfo')) {
			if(!$('#loginEmail').val().length) {
				if(Lockr.get('userInfo').email.length) {
		        	$('#loginEmail').val(Lockr.get('userInfo').email);
				}
			}
	    }

	    /////////////////
		///// FORM LOGIN
		/////////////////
		$('#formLogin').submit(function() {
			var $this = $(this);

			if (!$('#loginEmail').val().length) {
	            $('#loginEmail')
	                .attr('placeholder', 'Digite seu e-mail')
	                .focus();
	            return false;
	        }
	        if (!regex.email.test($('#loginEmail').val())) {
	            $('#loginEmail')
	                .val('')
	                .attr('placeholder', 'Digite um e-mail válido')
	                .focus();
	            return false;
	        }

	        if(!$this.hasClass('email-valido') && !$this.hasClass('sending')) {
	            
	            $.ajax({
	                url : url + 'login/validar-email',
	                method : "POST",
	                data : {
	                    email : $('#loginEmail').val()
	                },
	                beforeSend : function() {
	                    $this.addClass('sending');
	                    $('#inputEmail .control').addClass('is-loading');
	                    $('#submitLogin').val('. . .');
	                    
	                    $this.find('.notification')
	                    	.slideUp(200, function() {
	                    		$(this).removeClass('is-primary is-info is-success is-warning is-danger');
	                    	});
	                },
	                success : function(response) {
	                    $this.removeClass('sending');
	                    $('#inputEmail .control').removeClass('is-loading');

	                    setTimeout(function() {
		                    if (!response.action) {
		                    	$this.find('.notification p').html(response.message);
		                    	$this.find('.notification')
		                    		.addClass('is-warning')
		                    		.slideDown(200);
		                    	$('#submitLogin').val('C O N T I N U A R');
		                    } else {
		                    	Lockr.set('userInfo', {
						            email : $('#loginEmail').val()
						        });
		                    	$('#userInfo img').attr('src', response.user.foto);
		                    	$('#userInfo span').text('Olá ' + response.user.nome + ', tudo bem?');
		                    	$('#userInfo').show();
		                    	$('#inputEmail').find('.icon-email-valid').fadeIn(200);
		                    	$('#inputSenha').slideDown(200);
		                    	$('#loginSenha').focus();
		                    	$this.addClass('email-valido');
		                    	$('#loginEmail').prop('readonly', true);

		                    	$('#submitLogin').val('E N T R A R');
		                    }
	                    }, 200);
	                },
	                error : function() {
	                    setTimeout(function() {
	                    	$this.removeClass('sending');
	                    	$('#inputEmail .control').removeClass('is-loading');
		                    $this.find('.notification')
	                    		.addClass('is-danger')
	                    		.slideDown(200);
		                    $('#submitLogin').val('C O N T I N U A R');
	                    }, 200);
	                }
	            });
	        }

	        if (!$('#loginSenha').val().length) {
	            $('#loginSenha')
	                .attr('placeholder', 'Digite sua senha')
	                .focus();
	            return false;
	        }

			if($this.hasClass('email-valido') && !$this.hasClass('sending')) {
	            $.ajax({
	                url : url + 'login/autenticar',
	                method : "POST",
	                data : {
	                    email  	 : $('#loginEmail').val(),
	                    senha  	 : $.base64.encode($('#loginSenha').val()),
	                },
	                beforeSend : function() {
	                    $this.addClass('sending');
	                    $('#submitLogin').val('. . .');
	                    
	                    $this.find('.notification')
	                    	.slideUp(200, function() {
	                    		$(this).removeClass('is-primary is-info is-success is-warning is-danger');
	                    	});
	                },
	                success : function(response) {
	                    $this.removeClass('sending');

	                    setTimeout(function() {
		                    if (!response.action) {
		                    	$this.find('.notification p').html(response.message);
		                    	$this.find('.notification')
		                    		.addClass('is-danger')
		                    		.slideDown(200);
		                    	$('#submitLogin').val('C O N T I N U A R');
		                    } else {
		                    	$('#submitLogin').val('P R E P A R A N D O');
		                    	setTimeout(function() {
		                    		window.location.href = response.redirect;
		                    	}, 300);
		                    }
	                    }, 200);
	                },
	                error : function() {
	                    setTimeout(function() {
	                    	$this.removeClass('sending');
	                    	$('#inputEmail .control').removeClass('is-loading');
		                    $this.find('.notification')
	                    		.addClass('is-danger')
	                    		.slideDown(200);
		                    $('#submitLogin').val('C O N T I N U A R');
	                    }, 200);
	                }
	            });
	        }
		});
	});
</script>