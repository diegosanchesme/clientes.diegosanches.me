<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section id="logoutAdmin" class="hero is-fullheight">
	<div class="hero-head">
		<div class="container has-text-centered">
			<?php $this->load->view('templates/logo'); ?>
		</div>
	</div>
	<div class="hero-body">
		<div class="container has-text-centered">
			<h1>L O G O U T</h1>
		</div>
	</div>
	<div class="hero-foot">
		<div class="container has-text-centered">
			<a href="" class="">. . .</a>
		</div>
	</div>
</section>
<style type="text/css">
	#footer {
		display: none;
	}
	a {
		font-size: 14px;
		color: #999;
		letter-spacing: 1.5px;
	}
	a:hover {
		color: #CCC;
	}
	
	#logoutAdmin {
		background: #000;
	    background: -webkit-linear-gradient(-220deg, #000, #000);
	    background: linear-gradient(-220deg, #000, #000);
	}
	.hero-foot {}
	.hero-foot a {
		display: inline-block;
		position: relative;
		top: -25px;
	}
</style>