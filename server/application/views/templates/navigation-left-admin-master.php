<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<aside id="menuLeft" class="menu">
	<p class="menu-label">Geral</p>
	<ul class="menu-list">
		<li>
			<a href="<?php echo base_url('painel.html');?>">
				<i class="fa fa-home"></i>Painel
			</a>
		</li>
	</ul>
	
	
	<p class="menu-label">Clientes</p>
	<ul class="menu-list">
		<li>
			<a href="<?php echo base_url('clientes.html');?>">
				<i class="fa fa-users"></i>
				<span>Todos Clientes</span>
				<!-- <span class="push">12</span> -->
			</a>
		</li>
		<li>
			<a href="<?php echo base_url('clientes/novo.html');?>">
				<i class="fa fa-user-plus"></i>
				<span>Novo Cliente</span>
			</a>
		</li>
	</ul>


	<p class="menu-label">Financeiro</p>
	<ul class="menu-list">
		<li>
			<a href="<?php echo base_url();?>">
				<i class="fa fa-barcode"></i>
				<span>Enviar Cobrança</span>
			</a>
		</li>
	</ul>


	<p class="menu-label">Minha Conta</p>
	<ul class="menu-list">
		<li>
			<a href="<?php echo base_url('logout.html');?>">
				<i class="fa fa-sign-out"></i>Sair
			</a>
		</li>
	</ul>
	<p id="copyright">DIEGOSANCHES.ME © <?php echo date('Y'); ?><br>Versão 0.0.0</p>
</aside>