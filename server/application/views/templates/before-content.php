<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div id="main" class="">
	<header id="mainHeader">
		<nav class="breadcrumb" aria-label="breadcrumbs">
			<ul>
				<li>
					<a href="<?php echo base_url(); ?>">
						<i class="fa fa-home" style="margin-right: 7px;"></i>
						Painel
					</a>
				</li>
				<?php foreach ($breadcrumb as $value) : ?>
					<li><a href="<?php echo $value['url']; ?>"><?php echo $value['label']; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</nav>
		<div id="chat" class="icons-right">
			<i class="fa fa-comments"></i>
		</div>
		<div id="alerts" class="icons-right">
			<i class="fa fa-bell"></i>
		</div>
		<div id="ajuda" class="icons-right">
			<a href="<?php echo base_url();?>">
				<i class="fa fa-question"></i>
			</a>
		</div>
		<div id="logout" class="icons-right">
			<a href="<?php echo base_url('logout.html');?>">
				<i class="fa fa-sign-out"></i>
			</a>
		</div>
	</header>
	<section id="beforeContent" class="section">
		<div class="container">
			
			<?php if(!empty($message)) : ?>
			<div id="updateSuccess" class="notification <?php echo $message['type']; ?>">
				<button class="delete close-notifications"></button>
				<p><?php echo $message['text']; ?></p>
			</div>
			<?php endif; ?>

			<?php if(!$usuario->active) : ?>
			<div class="notification is-warning">
				<button class="delete close-notifications"></button>
				<h2><strong>AGUARDANDO CONFIRMAR SEU E-MAIL</strong></h2>
				<p><?php echo $usuario->nome; ?>, você precisa confirmar o seu e-mail, <a id="reenviarConfirmacaoEmail" data-email="<?php echo $usuario->email; ?>">clique aqui para reenviar a confirmação</a> para <?php echo $usuario->email; ?>.</p>
				<a class="button is-warning is-loading" style="display:none; position: absolute; top: 0; right: 35px;"></a>
			</div>
			<?php endif; ?>

		</div>
	</section>
	<style type="text/css">
		#updateSuccess {
	        
	    }
	</style>