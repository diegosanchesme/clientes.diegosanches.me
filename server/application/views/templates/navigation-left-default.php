<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<aside id="menuLeft" class="menu">
	<p class="menu-label">Geral</p>
	<ul class="menu-list">
		<li>
			<a href="<?php echo base_url('painel.html');?>">
				<i class="fa fa-home"></i>Painel
			</a>
		</li>
	</ul>
	
	<p class="menu-label">Minha Conta</p>
	<ul class="menu-list">
		<li>
			<a href="<?php echo base_url('logout.html');?>">
				<i class="fa fa-sign-out"></i>Sair
			</a>
		</li>
	</ul>
	<p id="copyright">DIEGOSANCHES.ME © <?php echo date('Y'); ?><br>Versão 0.0.0</p>
</aside>