<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<header id="navgationLeft">
	<div class="wrap">
		<div id="menuLogo">
			<?php $this->load->view('templates/logo'); ?>
		</div>
		
		<?php
		switch ($usuario->access_level) {
			case 'PARTNER':
				$this->load->view('templates/navigation-left-partner');
				break;

			case 'SUPPORT':
				$this->load->view('templates/navigation-left-support');
				break;

			case 'CLIENTE':
				$this->load->view('templates/navigation-left-cliente');
				break;
			
			case 'ADMIN_MASTER':
				$this->load->view('templates/navigation-left-admin-master');
				break;
			
			default:
				$this->load->view('templates/navigation-left-default');
				break;
		} ?>
	</div>
</header>
<style type="text/css">
	#navgationLeft {
		background-color: #000;
        color: #DDD;
        width: 330px;
        height: 100%;
		position: fixed;
		top: 0;
		left: 0;
		z-index: 99;
		box-shadow: 5px 5px 20px rgba(0,0,0,0.3);
	}
	#navgationLeft .wrap {
		padding: 10px 0;
	}
	#navgationLeft .menu {}
	#navgationLeft .menu ul {}
	#navgationLeft .menu ul li {
		position: relative;
	}
	#navgationLeft .menu ul li a {}
	#navgationLeft .menu ul li a i {
	    text-align: center;
		display: inline-block;
	    width: 25px;
		margin-right: 10px;
	}
	#navgationLeft #copyright {
		font-size: 14px;
	    color: #555;
	    text-align: center;
	    letter-spacing: 1px;
	    line-height: 17px;
	    padding: 50px 0;
	}
</style>