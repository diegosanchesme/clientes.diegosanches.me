<?php defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html class="no-js" lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex">
        <meta name="googlebot" content="noindex">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo $title_page; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="icon" href="https://engineshop.pro/wp-content/uploads/2017/09/cropped-logo-icone-big-32x32.png" type="image/png"/>
        <!-- Place favicon.ico in the root directory -->

        <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo base_url();?>public/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url();?>public/css/font-awesome.min.css">

        <link rel="stylesheet" href="<?php echo base_url();?>public/css/bulma-docs.css">
        <link rel="stylesheet" href="<?php echo base_url();?>public/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url();?>public/css/plugins.css">
        <link rel="stylesheet" href="<?php echo base_url();?>public/css/media-queries.css">
        
        <script src='https://code.responsivevoice.org/responsivevoice.js'></script>
        <script src="<?php echo base_url();?>public/js/vendor/modernizr-2.8.3.min.js"></script>
        <script src="<?php echo base_url();?>public/js/vendor/isMobile.min.js"></script>
        <script src="<?php echo base_url();?>public/js/vendor/lockr.min.js"></script>
        <script src="<?php echo base_url();?>public/js/vendor/moment.min.js"></script>
        <script src="<?php echo base_url();?>public/js/vendor/moment-with-locales.min.js"></script>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php echo base_url();?>public/js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script type="text/javascript">
            var url = '<?php echo base_url();?>';
            var urlApiFront = '<?php echo base_url();?>' + 'api-front/';
            moment.locale('pt-br');
        </script>
    </head>
    <body class="loading">
    	<!--[if lt IE 8]>
            <div class="browser-upgrade"></div>
        <![endif]-->
        
