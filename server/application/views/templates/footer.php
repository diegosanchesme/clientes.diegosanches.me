<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        
        <!-- END MAIN -->
        <footer id="footer" class="section">
            <div class="container">
                <p class="has-text-left is-size-7">DIEGOSANCHES.ME © <?php echo date('Y'); ?></p>
            </div>
        </footer>
        
        <!-- PLUGINS -->
        <script src="<?php echo base_url();?>public/js/vendor/jquery.base64.min.js"></script>
        <script src="<?php echo base_url();?>public/js/vendor/jquery.nicescroll.min.js"></script>
        <script src="<?php echo base_url();?>public/js/vendor/jquery.maskedinput.min.js"></script>
        <script src="<?php echo base_url();?>public/js/vendor/jquery.data-tables.min.js"></script>
        
        <script src="<?php echo base_url();?>public/js/plugins.js"></script>
        <!-- PLUGINS -->
        <script src="<?php echo base_url();?>public/js/ajax.js"></script>
        <script src="<?php echo base_url();?>public/js/main.js"></script>

        <?php /*
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-108403935-2"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
          gtag('config', 'UA-108403935-2');
        </script>
        */ ?>
    </body>
</html>