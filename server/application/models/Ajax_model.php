<?php 
    
    class Ajax_model extends CI_Model
    {

        public function __construct()
        {
            parent::__construct();


            // $this->load->library('user_agent');
            // $this->load->library('encryption');
            // $this->load->library('session');
            // $this->load->library('email');
            // $this->email->set_mailtype("html");

        }




        /**
         * Salvando usuário Pre Cadastro
         * @return Boolean
         */
        public function save_pre_cadastro($data = array())
        {
            if (empty($data))
                return false;

            return $this->db->insert('clientes', $data);
        }

    }