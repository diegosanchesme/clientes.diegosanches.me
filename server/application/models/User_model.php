<?php 
    
    class User_model extends CI_Model
    {

        public function __construct()
        {
            parent::__construct();

            
        }


        public function getInfo($id)
        {
            if(empty($id))
                return false;

            $getUser = $this->db
                ->select('id, nome, sobrenome, email, foto, first_access, status, active, access_level')
                ->get_where(
                    'admin_user',
                    array(
                        'id' => $id
                    )
                )->row();
            return $getUser;
        }

    }




    