<?php 
    
    class Login_model extends CI_Model
    {

        public function __construct()
        {
            parent::__construct();

            $this->load->library('encryption');
            $this->load->library('user_agent');

        }




        /**
         * Verifica se o email já esta cadastrado
         * @param String $email
         * @return Boolean
         */
        public function verificar_email($email = '')
        {
            if (empty($email))
                return NULL;

            $getPreCadastro = $this->db
                ->select('nome, email, foto, status')
                ->get_where(
                    'clientes',
                    array(
                        'email' => $email
                    )
                )->row();

            if (empty($getPreCadastro)) {
                $getPreCadastro = NULL;
            }

            return $getPreCadastro;
        }



        /**
         * Verifica se o email já esta cadastrado
         * @param String $email
         * @return Boolean
         */
        public function verificar_email_master($email = '')
        {
            if (empty($email))
                return NULL;

            $getPreCadastro = $this->db
                ->select('nome, email, foto, status')
                ->get_where(
                    'admin_user',
                    array(
                        'email' => $email
                    )
                )->row();

            if (empty($getPreCadastro)) {
                $getPreCadastro = NULL;
            }

            return $getPreCadastro;
        }




        /**
         * Verifica se o email e senha é válido
         * @param String $email
         * @param String $senha (base64 encoded)
         * @return Boolean || array
         */
        public function verificar_login($email = '', $senha = '')
        {
            if (empty($email) || empty($senha))
                return NULL;

            $getUser = $this->db
                ->select('id, email, senha')
                ->get_where(
                    'clientes',
                    array(
                        'email' => $email,
                        'status' => 1
                    )
                )->row();

            if(empty($getUser))
                return NULL;

            if (password_verify(base64_decode($senha), $getUser->senha)) {
                
                unset($getUser->senha);
                return $getUser;

            } else {
                return FALSE;
            }
        }




        /**
         * Verifica se o email e senha é válido
         * @param String $email
         * @param String $senha (base64 encoded)
         * @return Boolean || array
         */
        public function verificar_login_master($email = '', $senha = '')
        {
            if (empty($email) || empty($senha))
                return NULL;

            $getUser = $this->db
                ->select('id, email, senha')
                ->get_where(
                    'admin_user',
                    array(
                        'email' => $email,
                        'status' => 1
                    )
                )->row();

            if(empty($getUser))
                return NULL;

            if (password_verify(base64_decode($senha), $getUser->senha)) {
                
                unset($getUser->senha);
                return $getUser;

            } else {
                return FALSE;
            }
        }







    }