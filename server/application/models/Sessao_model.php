<?php 
    
    class Sessao_model extends CI_Model
    {

        public function __construct()
        {
            parent::__construct();

            $this->load->library('session');
            $this->load->library('email');
            $this->email->set_mailtype("html");

        }

        public function userId()
        {
            return $this->session->user->id;
        }

        public function usuario_logado()
        {
            return $this->session->logged_in;
        }

        public function id_sessao()
        {
            return $this->session->session_id;
        }

        public function verificar_login()
        {
            if (empty($this->session->user))
                redirect('/login/');
        }

        public function verificar_login_page()
        {
            if (!empty($this->session->user))
                redirect('/painel/');
        }

        public function iniciar($user = array())
        {
            if(empty($user))
                return FALSE;

            $this->session->set_userdata(array(
                'sessionId' => session_id(),
                'user' => $user
            ));
        }

        public function finalizar_login()
        {
            $this->session->sess_destroy();
        }

    }




    