<?php 

/**
 * Util Diego Sanches
 * Recursos e ultilidades para CodeIgniter
 *
 * @package     Util Diego Sanches
 * @subpackage  Libraries
 * @category    Libraries
 * @author      Diego Sanches
 * @link        https://diegosanches.me
 * @version     1.0.0
 */

class Util_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        //$this->load->helper('string');

        $this->load->library(
            array(
                'user_agent',
                'encryption',
                'session',
                'email'
            )
        );
        
        $this->email->set_mailtype("html");

    }

    



    /**
    * userInfo()
    * @description 
    * @param String JSON | JSON_STRING
    * @return Default Object [ mixed ]
    * @version 1.0.0
    */
    public function userInfo($format = NULL)
    {
        $userInfo = (Object) array(
            'IP'            => $this->input->ip_address(),
            'browser'       => $this->agent->browser().' - '.$this->agent->version(),
            'robot'         => $this->agent->robot(),
            'mobile'        => $this->agent->mobile(),
            'plataform'     => $this->agent->platform(),
            'referrer'      => $this->agent->referrer(),
            'agent_string'  => $this->agent->agent_string(),
        );

        switch ($format) {
            case 'JSON':
                $userInfo = json_encode($userInfo);
                break;

            case 'JSON_STRING':
                $userInfo = (String) json_encode($userInfo);
                break;
        }

        return $userInfo;
    }





    /**
    * apiReturn()
    * @description Método que retorna json com valores e output correto para requisições Ajax
    * @param Array $array Array com os valores a serem retornados
    * @return JSON
    * @version 1.0.0
    */
    public function apiReturn($array = array())
    {
        if(!isset($array['timestamp']))
            $array['timestamp'] = time();

        if(!isset($array['status']))
            $array['status'] = 200;

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($array));
    }





}