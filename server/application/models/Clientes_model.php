<?php 
    
    class Clientes_model extends CI_Model
    {

        public function __construct()
        {
            parent::__construct();
            $this->load->helper('string');

            $this->load->model('util_model', 'util', TRUE);

            

        }




        public function getAll()
        {
            $getAll = $this->db
                ->select('id, id_cliente, nome, sobrenome, email, foto, first_access, status, active, access_level')
                ->get_where(
                    'admin_user',
                    array(
                        'access_level' => 'CLIENTE'
                    )
                )->result();

            foreach ($getAll as $item) {
                $item->services = $this->getServicesCliente($item->id);
            }

            return $getAll;
        }


        public function getServicesCliente($id = NULL)
        {
            if(empty($id))
                return NULL;

            // $getServices = $this->db
            //     ->select()
            //     ->get_where()
            //     ->result();

            return array('id' => $id );
        }



        public function saveNew($data = array())
        {
            if(empty($data))
                return NULL;

            $data['id_cliente'] = $this->generateIdCliente();
            $data['date_create'] = date("Y-m-d H:i:s");
            $data['user_info'] = $this->util->userInfo('JSON_STRING');

            return $this->db->insert('admin_user', $data);;
        }




        public function generateIdCliente()
        {
            $idCliente = random_string('numeric', 5);

            $verifyIdCliente = $this->db
                ->select('id_cliente')
                ->get_where(
                    'admin_user',
                    array('id_cliente' => $idCliente)
                )->row();

            if(!empty($verifyIdCliente))
                $idCliente = $this->generateIdCliente();

            return $idCliente;
        }















    }