if(Lockr) {
    Lockr.prefix = 'MasterPanelAdmin_';
}

var _loadSpinner = 0;
var _loadCount = 0;
var defaultVoice = 'Brazilian Portuguese Female';


$(document).on('ready', function () {


    if($('#timerNow').length) {
        $('#timerNow').text(moment().format('LLL'));
    }
    setInterval(function() {
        $('#timerNow').text(moment().format('LLL'));
    }, 10000);




    $('#navgationLeft').niceScroll({
        cursorwidth         : 1,
        cursorcolor         : '#000',
        cursorborder        : 'none',
        cursorborderradius  : 'none',
        scrollspeed         : 30,
        zindex              : 999
    });


	$('.close-notifications').each(function() {
		var $this = $(this);
		$this.on('click', function() {
			$this.parent().slideUp(200, function() {
				$(this).find('p').html('');
				$(this).removeClass('is-primary is-info is-success is-warning is-danger');
			});
		});
	});



    $(window).on('scroll', function () {
        var _scaleY = $(document).scrollTop();
        
        if (_scaleY > 200) {
        	$('body').addClass('scrolling');
        	//$('#brandImg').attr('src', 'img/hotel-quando.png');
        } else {
            $('body').removeClass('scrolling scrolling-down scrolling-up');
        }
        
    });

    $('body').on('mousewheel', function(e) {
        if(e.originalEvent.wheelDelta / 120 > 0) {
            $('body').addClass('scrolling-up').removeClass('scrolling-down');
        } else {
            $('body').addClass('scrolling-down').removeClass('scrolling-up');
        }
    });
});



$(window).on('load', function() {

	setTimeout(function() {

        $('body').removeClass('loading');
        animaScroll(0);

    }, 500);

});



$(window).on('beforeunload', function() {
	$('body').addClass('loading');
    setTimeout(function() {
        window.location.reload();
    }, 5000);
});



$(window).on('resize', function() {

	
});