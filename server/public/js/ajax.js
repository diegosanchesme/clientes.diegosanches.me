$(document).on('ready', function () {

    $('#reenviarConfirmacaoEmail').on('click', function() {
        
        var $this = $(this);
        var email = $('#reenviarConfirmacaoEmail').attr('data-email');

        $.ajax({
            url : url + 'ajax/reenviar-email-confirmacao',
            method : "POST",
            data : {
                email    : email
            },
            beforeSend : function() {
                $this.parent().parent().find('.is-loading').show();
            },
            success : function(response) {
                
                $this.parent().parent().find('.is-loading').hide();

                if(response.sendEmail) {
                    $this.parent().parent().find('p').html('Em alguns minutos você vai receber em <strong>' + email + '</strong> o link para confirmar seu email.');
                }

            },
            error : function() {
                $this.parent().parent().find('.is-loading').hide();
            }
        });

    });
    
});