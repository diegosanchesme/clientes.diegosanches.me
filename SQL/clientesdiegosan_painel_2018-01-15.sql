# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: clientesdiegosan_painel
# Generation Time: 2018-01-16 01:17:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_sessions`;

CREATE TABLE `admin_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin_sessions` WRITE;
/*!40000 ALTER TABLE `admin_sessions` DISABLE KEYS */;

INSERT INTO `admin_sessions` (`id`, `ip_address`, `timestamp`, `data`)
VALUES
	('a15fa9d86cb8ed2244f6e0041a19a1404bb446f7','::1',1515531696,X'5F5F63695F6C6173745F726567656E65726174657C693A313531353533313639363B');

/*!40000 ALTER TABLE `admin_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_user`;

CREATE TABLE `admin_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `sobrenome` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `rg` varchar(16) DEFAULT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `foto` varchar(150) DEFAULT 'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',
  `senha` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `hash_key` varchar(255) DEFAULT NULL,
  `hash_salt` varchar(255) DEFAULT NULL,
  `first_access` int(11) DEFAULT '1',
  `status` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '0',
  `access_level` enum('MASTER','PARCEIRO','SUPORTE') NOT NULL DEFAULT 'SUPORTE',
  `user_info` text,
  `date_create` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;

INSERT INTO `admin_user` (`id`, `nome`, `sobrenome`, `email`, `rg`, `cpf`, `celular`, `telefone`, `foto`, `senha`, `token`, `hash_key`, `hash_salt`, `first_access`, `status`, `active`, `access_level`, `user_info`, `date_create`, `date_update`)
VALUES
	(1,'Diego','Sanches','hello@diegosanches.me',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png','$2y$10$7q/4EiCiKJ5KjzZxjCZPguM2cWCogm5/r81/cVwr7NQuhE94aPwE.','$99mpgjT93hlNI#!UfGtehgMrnN9LVqE4IRb','994c22bfdd003e43db9b422f17c9849d6052f51920ba2ae8fded49e8dd7fc87b',NULL,1,0,0,'MASTER','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-09 17:42:03','2018-01-09 17:43:30');

/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table clientes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `sobrenome` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `rg` varchar(16) DEFAULT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `foto` varchar(150) DEFAULT 'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',
  `senha` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `hash_key` varchar(255) DEFAULT NULL,
  `hash_salt` varchar(255) DEFAULT NULL,
  `first_access` int(11) DEFAULT '1',
  `status` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '0',
  `access_level` enum('CLIENTE') NOT NULL DEFAULT 'CLIENTE',
  `user_info` text,
  `date_create` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_cliente` (`id_cliente`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table servicos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `servicos`;

CREATE TABLE `servicos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tipo` enum('DIGITAL','GRAFICO','NUVEM','OUTROS') DEFAULT NULL,
  `cobranca` enum('HORA','DIARIA','MENSAL','SEMESTRAL','ANUAL','PACOTE','INDIVIDUAL') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table servicos_clientes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `servicos_clientes`;

CREATE TABLE `servicos_clientes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_servico` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table suporte_message
# ------------------------------------------------------------

DROP TABLE IF EXISTS `suporte_message`;

CREATE TABLE `suporte_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
