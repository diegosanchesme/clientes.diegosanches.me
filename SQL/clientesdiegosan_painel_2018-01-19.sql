# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.18)
# Database: clientesdiegosan_painel
# Generation Time: 2018-01-19 03:08:22 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_sessions`;

CREATE TABLE `admin_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin_sessions` WRITE;
/*!40000 ALTER TABLE `admin_sessions` DISABLE KEYS */;

INSERT INTO `admin_sessions` (`id`, `ip_address`, `timestamp`, `data`)
VALUES
	('5a23cff66b33f1bd2e812bc51ce7e09fa810d80a','::1',1515627781,X'5F5F63695F6C6173745F726567656E65726174657C693A313531353632373738303B');

/*!40000 ALTER TABLE `admin_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table admin_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin_user`;

CREATE TABLE `admin_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) DEFAULT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `sobrenome` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `rg` varchar(16) DEFAULT NULL,
  `cpf` varchar(14) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `telefone` varchar(15) DEFAULT NULL,
  `foto` varchar(150) DEFAULT 'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',
  `senha` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `hash_key` varchar(255) DEFAULT NULL,
  `hash_salt` varchar(255) DEFAULT NULL,
  `first_access` int(11) DEFAULT '1',
  `status` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '0',
  `access_level` enum('PARTNER','SUPPORT','ADMIN_MASTER','CLIENTE') NOT NULL DEFAULT 'CLIENTE',
  `user_info` text,
  `date_create` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_cliente` (`id_cliente`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;

INSERT INTO `admin_user` (`id`, `id_cliente`, `nome`, `sobrenome`, `email`, `rg`, `cpf`, `celular`, `telefone`, `foto`, `senha`, `token`, `hash_key`, `hash_salt`, `first_access`, `status`, `active`, `access_level`, `user_info`, `date_create`, `date_update`)
VALUES
	(1,83940,'Diego','Sanches','hello@diegosanches.me',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png','$2y$10$7q/4EiCiKJ5KjzZxjCZPguM2cWCogm5/r81/cVwr7NQuhE94aPwE.','$99mpgjT93hlNI#!UfGtehgMrnN9LVqE4IRb','994c22bfdd003e43db9b422f17c9849d6052f51920ba2ae8fded49e8dd7fc87b',NULL,1,1,1,'ADMIN_MASTER','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-05 13:31:00','2018-01-06 13:51:23'),
	(2,16394,'Bruno','Liguori Sia','contato@ibirah.com.br',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:32:51','2018-01-06 13:51:32'),
	(3,79374,'Silvio','Alterio','contato@damadeira.com.br',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:33:51','2018-01-06 13:51:39'),
	(4,64045,'Gustavo','Pera','contato@gustavopera.com',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:34:51','2018-01-06 13:51:43'),
	(5,81538,'Claudia','Vieira','contato@zelocontabilidade.com',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:35:51','2018-01-06 13:51:47'),
	(6,75492,'Atilio','de Paula','contato@senhordobonfino.com',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:36:51','2018-01-06 13:51:51'),
	(7,16284,'Marechal',NULL,'mcmarechal@vvar.com.br',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:37:51','2018-01-07 16:27:45'),
	(8,27359,'Edvaldo','Neto','contato@edvalduneto.com',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:38:51','2018-01-06 13:52:04'),
	(9,75634,'Marcio','Cipriano','contato@coachcipriano.com',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:39:51','2018-01-06 13:52:09'),
	(10,45256,'Vitor','Rodrigues','contato@pragadepoeta.com',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:40:51','2018-01-06 13:52:12'),
	(11,46732,'Sthéfanie','Carminha','contato@arteindependente.com',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:41:51','2018-01-06 13:52:15'),
	(12,13674,'Rafael','Camargo','rafael@altaicompany.com.br',NULL,NULL,NULL,NULL,'https://cdn.engineshop.pro/public/img/avatar-engineshop.png',NULL,NULL,NULL,NULL,1,0,0,'CLIENTE','{\"IP\":\"187.101.105.237\",\"browser\":\"Firefox - 57.0\",\"robot\":\"\",\"mobile\":\"\",\"plataform\":\"Mac OS X\",\"referrer\":\"https:\\/\\/engineshop.pro\\/\",\"agent_string\":\"Mozilla\\/5.0 (Macintosh; Intel Mac OS X 10.13; rv:57.0) Gecko\\/20100101 Firefox\\/57.0\"}','2018-01-06 12:42:51','2018-01-06 13:52:17');

/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table servicos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `servicos`;

CREATE TABLE `servicos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `tipo` enum('DIGITAL','GRAFICO','NUVEM','OUTROS') DEFAULT NULL,
  `cobranca` enum('HORA','DIARIA','MENSAL','SEMESTRAL','ANUAL','PACOTE','INDIVIDUAL') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;

INSERT INTO `servicos` (`id`, `nome`, `tipo`, `cobranca`)
VALUES
	(1,'Hospedagem','NUVEM','MENSAL'),
	(2,'Hotsite','DIGITAL','INDIVIDUAL'),
	(3,'E-mail','NUVEM','MENSAL'),
	(4,'Aplicativo','DIGITAL','INDIVIDUAL'),
	(5,'Suporte','OUTROS','PACOTE'),
	(6,'Dominio','OUTROS','ANUAL');

/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table servicos_clientes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `servicos_clientes`;

CREATE TABLE `servicos_clientes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_servico` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table suporte_message
# ------------------------------------------------------------

DROP TABLE IF EXISTS `suporte_message`;

CREATE TABLE `suporte_message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
